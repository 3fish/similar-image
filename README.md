# Similar Image #

"Similar Image" is a project which focuses on the detection of similar (or equal) Images in a defined collection of images.
The early version is apposed to Scan a folder for Images, compare each image with any other image and calculate a percentage value.
These Results are Displayed and allow the user to find and even delete duplicates or similar images.

## Contact Me ##

Dirk Peters

[Email](mailto:dpeters@3fish.org)