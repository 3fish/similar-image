
package gui.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;

/**
 * This is a special Layout which allows dynamic Component width/height
 * calculation. Components arr added by the extended add Method. Components
 * added this way are called "strong" components. "weak" components are
 * components which are added by the standart add function. These Components get
 * the left over width/height of the parrent Component. So they share the rest!
 * Example: Component cp; cp.setLayout(new
 * PercentageLayout(PercentageLayout.ORIENTATION_LEFT_TO_RIGHT));
 * cp.add(button1,"10%");//This Component tries to get 10% of the parrent
 * Component width. cp.add(button2,"100");//This Component tries to be allways
 * 100px wide. cp.add(button3,"square");//this components width is allways equal
 * to the height of its parrent. cp.add(button4);//This Component is "weak" and
 * its width is equal to the spare width divided by all other Components without
 * definition.
 * 
 * @author Dirk Peters
 */
public class PercentageLayout implements LayoutManager
{
	static public final int                       ORIENTATION_LEFT_TO_RIGHT = 0;
	static public final int                       ORIENTATION_RIGHT_TO_LEFT = 1;
	static public final int                       ORIENTATION_TOP_TO_BOTTOM = 2;
	static public final int                       ORIENTATION_BOTTOM_TO_TOP = 3;
	private final int                             orientation;
	ArrayList<Triple<Component, Double, Integer>> components                = new ArrayList<Triple<Component, Double, Integer>> ();
	
	/**
	 * Creates a new PercentageLayout
	 * 
	 * @param orientation
	 *            the Orientation in which the Components are ordered.
	 */
	public PercentageLayout (final int orientation)
	{
		this.orientation = orientation;
	}
	
	@Override
	public void addLayoutComponent (final String arg0, final Component arg1)
	{
		Double p = -1.0;
		int calculationType = 0;
		if (arg0 != null)
		{
			if (arg0.length () > 0)
			{
				if (arg0.matches ("^[0-9]*%$"))
				{
					final String argTMP = arg0.substring (0, arg0.indexOf ("%"));
					p = ( (Double.parseDouble (argTMP)) / 100);
					calculationType = 0;
				} else if (arg0.matches ("^[0-9]*$"))
				{
					p = Double.parseDouble (arg0);
					calculationType = 1;
				} else if (arg0.matches ("^square$"))
				{
					p = 0.0;
					calculationType = 2;
				}
			}
		}
		components.add (new Triple<Component, Double, Integer> (arg1, p, calculationType));
	}
	
	/**
	 * checks if the List of "strong" Components contains the given component.
	 * 
	 * @param c
	 * @return the index of the "strong" component or -1 if not existent.
	 */
	private int containsComponent (final Component c)
	{
		for (int i = 0; i < components.size (); i++)
		{
			if (components.get (i).getFirstElement ().equals (c))
			{
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public void layoutContainer (final Container arg0)
	{
		final Component[] comps = arg0.getComponents ();
		final Insets ins = arg0.getInsets ();
		final Dimension dim = arg0.getSize ();
		dim.setSize (dim.getWidth () - (ins.left + ins.right), dim.getHeight () - (ins.top + ins.bottom));
		// For All Horizontal oriented Layouts
		if ( (orientation == PercentageLayout.ORIENTATION_LEFT_TO_RIGHT) || (orientation == PercentageLayout.ORIENTATION_RIGHT_TO_LEFT))
		{
			int fillComponentWidth = 0;
			int componentWidth = 0;
			int fillCount = 0;
			for (final Component comp : comps)
			{
				final int comPos = containsComponent (comp);
				if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
				{
					if (components.get (comPos).getThirdElement () == 0)
					{
						componentWidth += Math
						        .floor (components.get (comPos).getSecondElement () * dim.width);
					} else if (components.get (comPos).getThirdElement () == 1)
					{
						componentWidth += Math.floor (components.get (comPos).getSecondElement ());
					} else if (components.get (comPos).getThirdElement () == 2)
					{
						componentWidth += Math.floor (dim.height);
					}
				} else
				{
					fillCount++;
				}
			}
			if (fillCount > 0)
			{
				fillComponentWidth = (int) Math.floor ( (dim.width - componentWidth) / fillCount);
			}
			final int rest = ( (dim.width - componentWidth) - (fillComponentWidth * fillCount));
			if (orientation == PercentageLayout.ORIENTATION_LEFT_TO_RIGHT)
			{
				int wOffset = 0;
				for (final Component comp : comps)
				{
					final int comPos = containsComponent (comp);
					if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
					{
						if (components.get (comPos).getThirdElement () == 0)
						{
							final int compWidth = (int) Math.floor (components.get (comPos)
							        .getSecondElement () * dim.width);
							comp.setBounds (wOffset, 0, compWidth, dim.height);
							wOffset += compWidth;
						} else if (components.get (comPos).getThirdElement () == 1)
						{
							final int compWidth = (int) Math.floor (components.get (comPos)
							        .getSecondElement ());
							comp.setBounds (wOffset, 0, compWidth, dim.height);
							wOffset += compWidth;
						} else if (components.get (comPos).getThirdElement () == 2)
						{
							final int compWidth = (int) Math.floor (dim.height);
							comp.setBounds (wOffset, 0, compWidth, dim.height);
							wOffset += compWidth;
						}
					} else
					{
						comp.setBounds (wOffset, 0, fillComponentWidth, dim.height);
						wOffset += fillComponentWidth;
					}
				}
			} else if (orientation == PercentageLayout.ORIENTATION_RIGHT_TO_LEFT)
			{
				int wOffset = dim.width - rest;
				for (final Component comp : comps)
				{
					final int comPos = containsComponent (comp);
					if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
					{
						if (components.get (comPos).getThirdElement () == 0)
						{
							final int compWidth = (int) Math.floor (components.get (comPos)
							        .getSecondElement () * dim.width);
							comp.setBounds (wOffset - compWidth, 0, compWidth, dim.height);
							wOffset -= compWidth;
						} else if (components.get (comPos).getThirdElement () == 1)
						{
							final int compWidth = (int) Math.floor (components.get (comPos)
							        .getSecondElement ());
							comp.setBounds (wOffset - compWidth, 0, compWidth, dim.height);
							wOffset -= compWidth;
						} else if (components.get (comPos).getThirdElement () == 2)
						{
							final int compWidth = (int) Math.floor (dim.height);
							comp.setBounds (wOffset - compWidth, 0, compWidth, dim.height);
							wOffset -= compWidth;
						}
					} else
					{
						comp.setBounds (wOffset - fillComponentWidth, 0, fillComponentWidth, dim.height);
						wOffset -= fillComponentWidth;
					}
				}
			}
		}
		// For all Vertical Oriented Layouts
		else if ( (orientation == PercentageLayout.ORIENTATION_BOTTOM_TO_TOP) || (orientation == PercentageLayout.ORIENTATION_TOP_TO_BOTTOM))
		{
			int fillComponentHeight = 0;
			int componentHeight = 0;
			int fillCount = 0;
			for (final Component comp : comps)
			{
				final int comPos = containsComponent (comp);
				if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
				{
					if (components.get (comPos).getThirdElement () == 0)
					{
						componentHeight += Math
						        .floor (components.get (comPos).getSecondElement () * dim.height);
					} else if (components.get (comPos).getThirdElement () == 1)
					{
						componentHeight += Math.floor (components.get (comPos).getSecondElement ());
					} else if (components.get (comPos).getThirdElement () == 2)
					{
						componentHeight += Math.floor (dim.width);
					}
				} else
				{
					fillCount++;
				}
			}
			if (fillCount > 0)
			{
				fillComponentHeight = (int) Math
				        .floor ( (dim.height - componentHeight) / fillCount);
			}
			final int rest = ( (dim.height - componentHeight) - (fillComponentHeight * fillCount));
			if (orientation == PercentageLayout.ORIENTATION_TOP_TO_BOTTOM)
			{
				int hOffset = 0;
				for (final Component comp : comps)
				{
					final int comPos = containsComponent (comp);
					if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
					{
						if (components.get (comPos).getThirdElement () == 0)
						{
							final int compHeight = (int) Math.floor (components.get (comPos)
							        .getSecondElement () * dim.height);
							comp.setBounds (0, hOffset, dim.width, compHeight);
							hOffset += compHeight;
						} else if (components.get (comPos).getThirdElement () == 1)
						{
							final int compHeight = (int) Math.floor (components.get (comPos)
							        .getSecondElement ());
							comp.setBounds (0, hOffset, dim.width, compHeight);
							hOffset += compHeight;
						} else if (components.get (comPos).getThirdElement () == 2)
						{
							final int compHeight = (int) Math.floor (dim.width);
							comp.setBounds (0, hOffset, dim.width, compHeight);
							hOffset += compHeight;
						}
					} else
					{
						comp.setBounds (0, hOffset, dim.width, fillComponentHeight);
						hOffset += fillComponentHeight;
					}
				}
			} else if (orientation == PercentageLayout.ORIENTATION_BOTTOM_TO_TOP)
			{
				int hOffset = dim.height - rest;
				for (final Component comp : comps)
				{
					final int comPos = containsComponent (comp);
					if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
					{
						if (components.get (comPos).getThirdElement () == 0)
						{
							final int compHeight = (int) Math.floor (components.get (comPos)
							        .getSecondElement () * dim.height);
							comp.setBounds (0, hOffset - compHeight, dim.width, compHeight);
							hOffset -= compHeight;
						} else if (components.get (comPos).getThirdElement () == 1)
						{
							final int compHeight = (int) Math.floor (components.get (comPos)
							        .getSecondElement ());
							comp.setBounds (0, hOffset - compHeight, dim.width, compHeight);
							hOffset -= compHeight;
						} else if (components.get (comPos).getThirdElement () == 2)
						{
							final int compHeight = (int) Math.floor (dim.width);
							comp.setBounds (0, hOffset - compHeight, dim.width, compHeight);
							hOffset -= compHeight;
						}
					} else
					{
						comp.setBounds (0, hOffset - fillComponentHeight, dim.width, fillComponentHeight);
						hOffset -= fillComponentHeight;
					}
				}
			}
		}
	}
	
	@Override
	public Dimension minimumLayoutSize (final Container arg0)
	{
		final Component[] comps = arg0.getComponents ();
		int biggestW = 0, biggestH = 0;
		int fillCount = 0;
		double fixedP = 0.0;
		for (final Component comp : comps)
		{
			final int comPos = containsComponent (comp);
			if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
			{
				fixedP += components.get (comPos).getSecondElement ();
			} else
			{
				fillCount++;
			}
		}
		final double fillP = 1.0 - fixedP;
		if ( (orientation == PercentageLayout.ORIENTATION_LEFT_TO_RIGHT) || (orientation == PercentageLayout.ORIENTATION_RIGHT_TO_LEFT))
		{
			for (final Component comp : comps)
			{
				final Dimension comDim = comp.getMinimumSize ();
				if (biggestH < comDim.height)
				{
					biggestH = comDim.height;
				}
				final int comPos = containsComponent (comp);
				int calcW = 0;
				if ( (comPos < 0) || ( (comPos >= 0) && (components.get (comPos)
				        .getSecondElement () < 0)))
				{
					calcW = (int) Math.ceil ( ( (comDim.width * fillCount) / (fillP * 100)) * 100);
				} else
				{
					calcW = (int) Math.ceil ( ( (comDim.width) / (components.get (comPos)
					        .getSecondElement () * 100)) * 100);
				}
				if (calcW > biggestW)
				{
					biggestW = calcW;
				}
			}
		} else
		{
			for (final Component comp : comps)
			{
				final Dimension comDim = comp.getMinimumSize ();
				if (biggestW < comDim.width)
				{
					biggestW = comDim.width;
				}
				final int comPos = containsComponent (comp);
				int calcH = 0;
				if ( (comPos < 0) || ( (comPos >= 0) && (components.get (comPos)
				        .getSecondElement () < 0)))
				{
					calcH = (int) Math.ceil ( ( (comDim.height * fillCount) / (fillP * 100)) * 100);
				} else
				{
					calcH = (int) Math.ceil ( ( (comDim.height) / (components.get (comPos)
					        .getSecondElement () * 100)) * 100);
				}
				if (calcH > biggestW)
				{
					biggestW = calcH;
				}
			}
		}
		return new Dimension (biggestW, biggestH);
	}
	
	@Override
	public Dimension preferredLayoutSize (final Container arg0)
	{
		final Component[] comps = arg0.getComponents ();
		int biggestW = 0, biggestH = 0;
		int fillCount = 0;
		double fixedP = 0.0;
		for (final Component comp : comps)
		{
			final int comPos = containsComponent (comp);
			if ( (comPos >= 0) && (components.get (comPos).getSecondElement () >= 0))
			{
				fixedP += components.get (comPos).getSecondElement ();
			} else
			{
				fillCount++;
			}
		}
		final double fillP = 1.0 - fixedP;
		if ( (orientation == PercentageLayout.ORIENTATION_LEFT_TO_RIGHT) || (orientation == PercentageLayout.ORIENTATION_RIGHT_TO_LEFT))
		{
			for (final Component comp : comps)
			{
				final Dimension comDim = comp.getPreferredSize ();
				if (biggestH < comDim.height)
				{
					biggestH = comDim.height;
				}
				final int comPos = containsComponent (comp);
				int calcW = 0;
				if ( (comPos < 0) || ( (comPos >= 0) && (components.get (comPos)
				        .getSecondElement () < 0)))
				{
					calcW = (int) Math.ceil ( ( (comDim.width * fillCount) / (fillP * 100)) * 100);
				} else
				{
					calcW = (int) Math.ceil ( ( (comDim.width) / (components.get (comPos)
					        .getSecondElement () * 100)) * 100);
				}
				if (calcW > biggestW)
				{
					biggestW = calcW;
				}
			}
		} else
		{
			for (final Component comp : comps)
			{
				final Dimension comDim = comp.getPreferredSize ();
				if (biggestW < comDim.width)
				{
					biggestW = comDim.width;
				}
				final int comPos = containsComponent (comp);
				int calcH = 0;
				if ( (comPos < 0) || ( (comPos >= 0) && (components.get (comPos)
				        .getSecondElement () < 0)))
				{
					calcH = (int) Math.ceil ( ( (comDim.height * fillCount) / (fillP * 100)) * 100);
				} else
				{
					calcH = (int) Math.ceil ( ( (comDim.height) / (components.get (comPos)
					        .getSecondElement () * 100)) * 100);
				}
				if (calcH > biggestH)
				{
					biggestH = calcH;
				}
			}
		}
		return new Dimension (biggestW, biggestH);
	}
	
	@Override
	public void removeLayoutComponent (final Component arg0)
	{
		final int i = containsComponent (arg0);
		if (i >= 0)
		{
			components.remove (i);
		}
	}
}
