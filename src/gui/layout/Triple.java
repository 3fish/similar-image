
package gui.layout;

/**
 * A Class which adopts the principle of the Class Pair but has 3 instead of two
 * elements.
 * 
 * @author Dirk Peters
 * @param <A>
 *            the first element
 * @param <B>
 *            the second element
 * @param <C>
 *            the third element
 */
public class Triple<A, B, C>
{
	private A a;
	private B b;
	private C c;
	
	/**
	 * Creates a new Triple
	 */
	public Triple ()
	{}
	
	/**
	 * Creates a new Triple and fills the elements with the Given ones.
	 * 
	 * @param firstElement
	 * @param secondElement
	 * @param thirdElement
	 */
	public Triple (final A firstElement, final B secondElement, final C thirdElement)
	{
		setFirstElement (firstElement);
		setSecondElement (secondElement);
		setThirdElement (thirdElement);
	}
	
	/**
	 * returns the first element
	 * 
	 * @return
	 */
	public A getFirstElement ()
	{
		return this.a;
	}
	
	/**
	 * returns the second element
	 * 
	 * @return
	 */
	public B getSecondElement ()
	{
		return this.b;
	}
	
	/**
	 * returns the third element
	 * 
	 * @return
	 */
	public C getThirdElement ()
	{
		return this.c;
	}
	
	/**
	 * sets the first element
	 * 
	 * @param firstElement
	 */
	public void setFirstElement (final A firstElement)
	{
		this.a = firstElement;
	}
	
	/**
	 * sets the second element
	 * 
	 * @param secondElement
	 */
	public void setSecondElement (final B secondElement)
	{
		this.b = secondElement;
	}
	
	/**
	 * sets the third element
	 * 
	 * @param thirdElement
	 */
	public void setThirdElement (final C thirdElement)
	{
		this.c = thirdElement;
	}
}
