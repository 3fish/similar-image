
package gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

/**
 * This Class is the Opening Window. It displays a simple selection mask to
 * define what folder should be searched and if the SearchThread should find
 * exact Matches only. It also contains a start Button to initiate the search.
 * 
 * @author Dirk Peters
 */
public class SIGUI extends JFrame
{
	public static void main (final String args[])
	{
		new SIGUI ();
	}
	
	/**
	 *
	 */
	private static final long serialVersionUID = -864835082253801238L;
	JTextField                pathField        = new JTextField ();
	JButton                   browsFolder      = new JButton ("...");
	JButton                   search           = new JButton ("Find");
	JCheckBox                 findMatches      = new JCheckBox ("exact Matches only");
	
	/**
	 * creates a new SIGUI object
	 */
	public SIGUI ()
	{
		super ("Similar Image");
		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		final Container cp = getContentPane ();
		cp.setLayout (new FlowLayout (FlowLayout.RIGHT));
		cp.add (pathField);
		cp.add (browsFolder);
		cp.add (findMatches);
		cp.add (search);
		cp.setPreferredSize (new Dimension (350, 50));
		pathField.setPreferredSize (new Dimension (300, 20));
		browsFolder.setPreferredSize (new Dimension (30, 20));
		findMatches.setPreferredSize (new Dimension (200, 20));
		search.setPreferredSize (new Dimension (100, 20));
		browsFolder.addActionListener (new ActionListener () {
			@Override
			public void actionPerformed (final ActionEvent arg0)
			{
				browseForFolder ();
			}
		});
		search.addActionListener (new ActionListener () {
			@Override
			public void actionPerformed (final ActionEvent arg0)
			{
				startSearch ();
			}
		});
		pack ();
		setResizable (false);
		setVisible (true);
	}
	
	/**
	 * a Helper Method which opens a folder Selection and waits for it to close.
	 * The Selected Path is then entered into the "Path" field.
	 */
	protected void browseForFolder ()
	{
		final JFileChooser fc = new JFileChooser ();
		fc.setFileSelectionMode (JFileChooser.DIRECTORIES_ONLY);
		fc.setCurrentDirectory (FileSystemView.getFileSystemView ().getRoots ()[0]);
		fc.setDialogTitle ("Select Folder for Search");
		final int returnVal = fc.showDialog (this, "Select Folder");
		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			pathField.setText (fc.getSelectedFile ().getAbsolutePath ());
		}
	}
	
	/**
	 * a Helper Method which prepares and starts the "SearchThread"
	 */
	protected void startSearch ()
	{
		final String pathToFolder = pathField.getText ();
		final File folder = new File (pathToFolder);
		if (folder.isDirectory () && folder.exists ())
		{
			final boolean exactMatch = findMatches.isSelected ();
			new SearchThread (this, exactMatch, folder, 0.0);
		}
	}
}
