
package gui;

import gui.layout.PercentageLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import compare.ImageWithInfo;
import compare.Match;

/**
 * This Class Represents a Window after successfully searching for Images and
 * Matching them. The window contains a list of all Matches(Not Images) and two
 * ImagePanels which display the Images of the currently selected Match. The
 * window also provides a Delete Button for each Image. Thees Buttons dont just
 * remove the Image from the Filesystem but also remove all Matches that contain
 * the Image.
 * 
 * @author Dirk Peters
 */
public class MatchDialog extends JDialog
{
	/**
	 *
	 */
	private static final long serialVersionUID = -4117749774800493587L;
	JPanel                    left_bottom      = new JPanel (new BorderLayout ());
	JPanel                    right_bottom     = new JPanel (new BorderLayout ());
	JPanel                    left             = new JPanel (new PercentageLayout (PercentageLayout.ORIENTATION_TOP_TO_BOTTOM));
	JPanel                    right            = new JPanel (new PercentageLayout (PercentageLayout.ORIENTATION_TOP_TO_BOTTOM));
	JPanel                    center           = new JPanel (new PercentageLayout (PercentageLayout.ORIENTATION_TOP_TO_BOTTOM));
	JPanel                    deleteBTs        = new JPanel (new PercentageLayout (PercentageLayout.ORIENTATION_LEFT_TO_RIGHT));
	JButton                   deleteA          = new JButton ("< DELETE");
	JButton                   deleteB          = new JButton ("DELETE >");
	JButton                   close            = new JButton ("CLOSE");
	DefaultListModel<Match>   dlm              = new DefaultListModel<Match> ();
	JList<Match>              matchList        = new JList<Match> (dlm);
	JScrollPane               scroll           = new JScrollPane (matchList);
	ImagePanel                imgPanelA        = new ImagePanel ();
	ImagePanel                imgPanelB        = new ImagePanel ();
	
	/**
	 * Creates a new MatchDialog which displays the given Matches
	 * 
	 * @param matches
	 *            ArrayList of Matches
	 */
	public MatchDialog (final ArrayList<Match> matches)
	{
		final Container cp = getContentPane ();
		setDefaultCloseOperation (WindowConstants.DISPOSE_ON_CLOSE);
		cp.setLayout (new PercentageLayout (PercentageLayout.ORIENTATION_LEFT_TO_RIGHT));
		cp.add (left);
		cp.add (center, "200");
		cp.add (right);
		left.add (imgPanelA);
		left.add (left_bottom, "100");
		right.add (imgPanelB);
		right.add (right_bottom, "100");
		center.add (scroll);
		center.add (deleteBTs, "50");
		center.add (close, "50");
		deleteBTs.add (deleteA, "50%");
		deleteBTs.add (deleteB, "50%");
		Collections.sort (matches);
		Collections.reverse (matches);
		for (int i = 0; i < matches.size (); i++)
		{
			dlm.addElement (matches.get (i));
		}
		matchList.addListSelectionListener (new ListSelectionListener () {
			@Override
			public void valueChanged (final ListSelectionEvent e)
			{
				if (!e.getValueIsAdjusting ())
				{
					selectionChanged ();
				}
			}
		});
		final ActionListener l = new ActionListener () {
			@Override
			public void actionPerformed (final ActionEvent e)
			{
				deleteImage ((JButton) e.getSource ());
			}
		};
		deleteA.addActionListener (l);
		deleteB.addActionListener (l);
		close.addActionListener (new ActionListener () {
			@Override
			public void actionPerformed (final ActionEvent e)
			{
				closeWindow ();
			}
		});
		((DefaultListCellRenderer) matchList.getCellRenderer ())
		        .setHorizontalAlignment (SwingConstants.CENTER);
		matchList.setBackground (Color.decode ("#999999"));
		imgPanelA.setBackground (Color.decode ("#FFAAAA"));
		deleteA.setBackground (Color.decode ("#FF9999"));
		left_bottom.setBackground (Color.decode ("#FFAAAA"));
		imgPanelB.setBackground (Color.decode ("#AAFFAA"));
		deleteB.setBackground (Color.decode ("#99FF99"));
		right_bottom.setBackground (Color.decode ("#AAFFAA"));
		close.setBackground (Color.decode ("#AAAAAA"));
		pack ();
		this.setBounds (GraphicsEnvironment.getLocalGraphicsEnvironment ()
		        .getMaximumWindowBounds ());
		setVisible (true);
	}
	
	/**
	 * closes the MatchDialog
	 */
	protected void closeWindow ()
	{
		dispatchEvent (new WindowEvent (this, WindowEvent.WINDOW_CLOSING));
	}
	
	/**
	 * Called when on of the two Delete Buttons is pressed. Removes the Image
	 * from the Filesystem and, if successful, removes all Matches containing
	 * the deleted Image from the Matchlist.
	 * 
	 * @param source
	 */
	protected void deleteImage (final JButton source)
	{
		ImageWithInfo img;
		if (source == deleteA)
		{
			img = matchList.getSelectedValue ().getImageA ();
		} else
		{
			img = matchList.getSelectedValue ().getImageB ();
		}
		int nextToSelect = matchList.getSelectedIndex ();
		if (img.deleteImageFile ())
		{
			for (int i = 0; i < dlm.getSize (); i++)
			{
				if (dlm.get (i).contains (img))
				{
					dlm.remove (i);
					nextToSelect--;
					i--;
				}
			}
		}
		matchList.setSelectedIndex (nextToSelect);
		matchList.revalidate ();
	}
	
	/**
	 * Called when the selected Match has changed. loads and displayes the
	 * Images from the newly selected Match.
	 */
	protected void selectionChanged ()
	{
		final int selPos = matchList.getSelectedIndex ();
		if (selPos == -1)
		{
			return;
		}
		try
		{
			imgPanelA.setImage (matchList.getSelectedValue ().getImageA ().getBufferedImage ());
			imgPanelB.setImage (matchList.getSelectedValue ().getImageB ().getBufferedImage ());
			this.repaint ();
		} catch (final IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace ();
		}
	}
}
