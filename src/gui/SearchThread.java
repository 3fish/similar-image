
package gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import compare.FullCompare;
import compare.ImageCompare;
import compare.ImageWithInfo;
import compare.Match;

/**
 * This Class represents two things. First it is a "simple" loading Window which
 * displays a progressbar and some infos about the progress. Secondly it
 * contains a Thread which processes the Image Search and Matching. (1) look for
 * the folder to look in. (2) search for images inside the folder and create a
 * "ImageWithInfo"-Object for each. (3) Calculate number of Comparisons. (Gaus
 * Sum Formula) (4) Compare each Image with any other Image and save Matches
 * into a list. (5) Open new "MatchDialog" with all found Matches .
 * 
 * @author Dirk Peters
 */
public class SearchThread extends JDialog implements Runnable
{
	/**
	 *
	 */
	private static final long serialVersionUID = -4717149297781009895L;
	private final boolean     exactMatch;
	private final File        folder;
	private volatile boolean  run              = true;
	private volatile long     time;
	private final double      minimumPercentage;
	JProgressBar              progressBar      = new JProgressBar ();
	JButton                   cancelBt         = new JButton ("Cancel");
	
	/**
	 * Create a new "SearchThread"
	 * 
	 * @param owner
	 *            the Frame owner to make this Dialog modal
	 * @param exactMatch
	 *            only exact matches in the result List?
	 * @param folder
	 *            target folder
	 * @param minimumPercentage
	 *            What percentage value has to be reached for an Image to be
	 *            added to the result list. Value between 0 and 1.
	 */
	public SearchThread (final Frame owner, final boolean exactMatch, final File folder, final double minimumPercentage)
	{
		super (owner, true);
		this.exactMatch = exactMatch;
		this.folder = folder;
		this.minimumPercentage = minimumPercentage;
		setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener (new WindowListener () {
			@Override
			public void windowActivated (final WindowEvent arg0)
			{}
			
			@Override
			public void windowClosed (final WindowEvent arg0)
			{}
			
			@Override
			public void windowClosing (final WindowEvent arg0)
			{
				cancelClicked ();
			}
			
			@Override
			public void windowDeactivated (final WindowEvent arg0)
			{}
			
			@Override
			public void windowDeiconified (final WindowEvent arg0)
			{}
			
			@Override
			public void windowIconified (final WindowEvent arg0)
			{}
			
			@Override
			public void windowOpened (final WindowEvent arg0)
			{
				startThread ();
			}
		});
		final Container cp = getContentPane ();
		cp.setLayout (new FlowLayout (FlowLayout.CENTER));
		cp.add (progressBar);
		cp.add (cancelBt);
		progressBar.setMinimum (0);
		progressBar.setStringPainted (true);
		progressBar.setPreferredSize (new Dimension (300, 20));
		cancelBt.setPreferredSize (new Dimension (100, 20));
		cp.setPreferredSize (new Dimension (350, 60));
		cancelBt.addActionListener (new ActionListener () {
			@Override
			public void actionPerformed (final ActionEvent arg0)
			{
				cancelClicked ();
			}
		});
		pack ();
		setResizable (false);
		setVisible (true);
	}
	
	/**
	 * Creates new "SearchThread" with standart Initialization Values. This is
	 * the Same as calling: new SearchThread(owner, false, folder, 0.85);
	 *
	 * @param owner
	 *            the Frame owner to make this Dialog modal
	 * @param folder
	 *            target folder
	 */
	public SearchThread (final Frame owner, final File folder)
	{
		this (owner, false, folder, 0.85);
	}
	
	/**
	 * Internal Method which is called when the "Cancel"-Button is clicked! This
	 * stops the thread befor it is done! No MatchDialog is opend if the thread
	 * was Canceled!
	 */
	protected void cancelClicked ()
	{
		run = false;
	}
	
	/**
	 * Helper Method to Compare two "ImageWithInfo"-Objects. This method either
	 * calls "compareImages" or "isExactMatch" from the ImageCompare Class.
	 * 
	 * @param img1
	 * @param img2
	 * @return a value between 0 and 1
	 * @throws IOException
	 */
	private double compareImages (final ImageWithInfo img1, final ImageWithInfo img2)
	        throws IOException
	{
		final ImageCompare cmp = new FullCompare (img1, img2);
		if (exactMatch)
		{
			return (cmp.isExactMatch ())? 1 : 0;
		} else
		{
			return cmp.compareImages ();
		}
	}
	
	/**
	 * Helper Method which searches and adds all Images in the global defined
	 * folder. Images are all files with the extension jpg, jpeg, png or bmp.
	 * 
	 * @return an Array of "ImageWithInfo"-objects
	 */
	private ImageWithInfo[] getAllImages ()
	{
		final ArrayList<ImageWithInfo> images = new ArrayList<ImageWithInfo> ();
		for (final File file : folder.listFiles ())
		{
			final String filename = file.getName ().toLowerCase ();
			final String ext = filename.substring (filename.lastIndexOf (".") + 1);
			if (file.isFile () && (ext.equals ("jpg") || ext.equals ("jpeg") || ext.equals ("png") || ext
			        .equals ("bmp")))
			{
				images.add (new ImageWithInfo (file));
			}
		}
		return images.toArray (new ImageWithInfo[0]);
	}
	
	/**
	 * This method is called to update the Progressbar and the Infortext (time
	 * left, matches left).
	 * 
	 * @param progress
	 *            the matches made since the last update.
	 */
	private synchronized void increaseProgress (final int progress)
	{
		long dif = System.currentTimeMillis () - time;
		progressBar.setValue (progressBar.getValue () + progress);
		final int rest = progressBar.getMaximum () - progressBar.getValue ();
		if (progressBar.getValue () > 0)
		{
			dif /= progressBar.getValue ();
			dif = dif * rest;
			final long second = (dif / 1000) % 60;
			final long minute = (dif / (1000 * 60)) % 60;
			final long hour = (dif / (1000 * 60 * 60)) % 24;
			final String formattedTime = String.format ("%02d:%02d:%02d", hour, minute, second);
			progressBar
			        .setString (progressBar.getValue () + " of " + progressBar.getMaximum () + " (" + formattedTime + ")");
		}
	}
	
	@Override
	public void run ()
	{
		final ArrayList<Match> matches = new ArrayList<Match> ();
		final ImageWithInfo[] images = getAllImages ();
		final int sum = ( ( (images.length - 1) * (images.length - 1)) + (images.length - 1)) / 2;// Gauss
		// Sum
		// formula
		progressBar.setMaximum (sum);
		time = System.currentTimeMillis ();
		run = true;
		int progress = 0;
		final long lastUpdate = System.currentTimeMillis ();
		for (int a = 0; (a < (images.length - 1)) && run; a++)
		{
			for (int b = a + 1; (b < images.length) && run; b++)
			{
				progress++;
				if ( (System.currentTimeMillis () - lastUpdate) > 500)
				{
					increaseProgress (progress);
					progress = 0;
				}
				try
				{
					final double matchingVal = compareImages (images[a], images[b]);
					if (matchingVal >= minimumPercentage)
					{
						matches.add (new Match (images[a], images[b], matchingVal));
					}
				} catch (final IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace ();
				}
			}
		}
		setVisible (false);
		if (run)
		{
			new MatchDialog (matches);
		}
		dispose ();
	}
	
	protected void startThread ()
	{
		final Thread t = new Thread (this);
		t.start ();
	}
}
