
package gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 * This Class Represents a Display for an Image. Any given Image will be
 * automaticaly rescaled and repositioned to fit inside this Panel. The Aspect
 * Ratio is not changed, the empty space is drawn with the defined Background
 * Color.
 * 
 * @author Dirk Peters
 */
public class ImagePanel extends JPanel
{
	private static final long serialVersionUID = -4932757001608438221L;
	private BufferedImage     img;
	
	/**
	 * new empty ImagePanel
	 */
	public ImagePanel ()
	{
		super ();
	}
	
	/**
	 * creates a new ImagePanel and displayes the given Image.
	 * 
	 * @param img
	 *            Image
	 */
	public ImagePanel (final BufferedImage img)
	{
		this ();
		setImage (img);
	}
	
	@Override
	public void paintComponent (final Graphics g)
	{
		super.paintComponent (g);
		if (img != null)
		{
			final double wScale = ( (100.0 / img.getWidth ()) * getWidth ()) / 100.0;
			final double hScale = ( (100.0 / img.getHeight ()) * getHeight ()) / 100.0;
			if (hScale >= wScale)
			{
				final int wPad = (int) Math.floor ( (getWidth () - (img.getWidth () * wScale)) / 2);
				final int hPad = (int) Math
				        .floor ( (getHeight () - (img.getHeight () * wScale)) / 2);
				g.drawImage (img, wPad, hPad, (int) Math.floor (img.getWidth () * wScale), (int) Math
				        .floor (img.getHeight () * wScale), this);
			} else
			{
				final int wPad = (int) Math.floor ( (getWidth () - (img.getWidth () * hScale)) / 2);
				final int hPad = (int) Math
				        .floor ( (getHeight () - (img.getHeight () * hScale)) / 2);
				g.drawImage (img, wPad, hPad, (int) Math.floor (img.getWidth () * hScale), (int) Math
				        .floor (img.getHeight () * hScale), this);
			}
		}
	}
	
	/**
	 * Sets the Image which is displayed in this Component.
	 * 
	 * @param img
	 */
	public void setImage (final BufferedImage img)
	{
		this.img = img;
	}
	
	@Override
	public void update (final Graphics g)
	{
		paint (g);
	}
}
