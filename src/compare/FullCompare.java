
package compare;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * This Class Compares two given Image based on their pixel Value distance. this
 * is very prcice but expansiv in terms of cpu usage. It does so by scaling both
 * images down to a size where the width and height of the Image are 100 px or
 * less. The Aspect Ratio is kept! After that the Images are compared Pixel by
 * Pixel. (The Intense part!)
 *
 * @author Dirk Peters
 */
public class FullCompare extends ImageCompare
{
	/**
	 * Creates a new "FullCompare" Object with two Images for Comparison. These
	 * Images are Scaled down for faster processing.
	 * 
	 * @param img1
	 * @param img2
	 * @throws IOException
	 *             is thrown if there is a problem while loading the images
	 */
	public FullCompare (final ImageWithInfo img1, final ImageWithInfo img2) throws IOException
	{
		final BufferedImage buf1 = img1.getBufferedImage ();
		final BufferedImage buf2 = img2.getBufferedImage ();
		if (buf1.getWidth () > buf1.getHeight ())
		{
			final double scale = (100.0 / buf1.getWidth ());
			bufImg1 = resizeImage (buf1, (int) (buf1.getWidth () * scale), (int) (buf1.getHeight () * scale));
		} else
		{
			final double scale = (100.0 / buf1.getHeight ());
			bufImg1 = resizeImage (buf1, (int) (buf1.getWidth () * scale), (int) (buf1.getHeight () * scale));
		}
		if (buf2.getWidth () > buf2.getHeight ())
		{
			final double scale = (100.0 / buf2.getWidth ());
			bufImg2 = resizeImage (buf2, (int) (buf2.getWidth () * scale), (int) (buf2.getHeight () * scale));
		} else
		{
			final double scale = (100.0 / buf2.getHeight ());
			bufImg2 = resizeImage (buf2, (int) (buf2.getWidth () * scale), (int) (buf2.getHeight () * scale));
		}
	}
	
	/**
	 * Compares two Images Pixelwise. Returns a double reaching from 0 (no
	 * match) to 1 (100% match).
	 *
	 * @param img1
	 * @param img2
	 * @return a percentage value reaching from 0 to 1.
	 */
	private double compareImageByPixels (final BufferedImage img1, final BufferedImage img2)
	{
		double result = 0;
		if (haveSameSize (img1, img2))
		{
			for (int x = 0; x < img1.getWidth (); x++)
			{
				for (int y = 0; y < img1.getHeight (); y++)
				{
					result += comparePixel (img1.getRGB (x, y), img2.getRGB (x, y));
				}
			}
			result /= (img1.getWidth () * img1.getHeight ());
		}
		return result;
	}
	
	@Override
	public double compareImages ()
	{
		if (bufImg1.getWidth () >= bufImg2.getWidth ())
		{
			if (bufImg1.getWidth () == bufImg2.getWidth ())
			{
				if (bufImg1.getHeight () > bufImg2.getHeight ())
				{
					return compareImageByPixels (resizeImage (bufImg1, bufImg2.getWidth (), bufImg2.getHeight ()), bufImg2);
				} else if (bufImg1.getHeight () < bufImg2.getHeight ())
				{
					return compareImageByPixels (resizeImage (bufImg2, bufImg1.getWidth (), bufImg1.getHeight ()), bufImg1);
				}
			} else
			{
				return compareImageByPixels (resizeImage (bufImg1, bufImg2.getWidth (), bufImg2.getHeight ()), bufImg2);
			}
		} else
		{
			return compareImageByPixels (resizeImage (bufImg2, bufImg1.getWidth (), bufImg1.getHeight ()), bufImg1);
		}
		return compareImageByPixels (bufImg1, bufImg2);
	}
	
	/**
	 * This Methos Compares two Pixel based on there Color distanc. the
	 * returning value is 0 for the maximum distance in an rgb Color space and 1
	 * if the colors match compleetly.
	 * 
	 * @param pixelA
	 * @param pixelB
	 * @return a double value reaching from 0 to 1
	 */
	private double comparePixel (final int pixelA, final int pixelB)
	{
		final Color colA = new Color (pixelA);
		final Color colB = new Color (pixelB);
		final double deltaE_RGB = Math.sqrt ( (Math.pow (colA.getRed () - colB.getRed (), 2) + Math
		        .pow (colA.getGreen () - colB.getGreen (), 2) + Math.pow (colA.getBlue () - colB
		        .getBlue (), 2)));
		return 1 - (deltaE_RGB / 255);
	}
	
	/**
	 * This Method returns true if the two given Images have the same dimensions
	 * 
	 * @param img1
	 * @param img2
	 * @return true if they have the same dimensions, otherwise false.
	 */
	private boolean haveSameSize (final BufferedImage img1, final BufferedImage img2)
	{
		if ( (img1.getWidth () == img2.getWidth ()) && (img1.getHeight () == img2.getHeight ()))
		{
			return true;
		}
		return false;
	}
	
	@Override
	public boolean isExactMatch ()
	{
		if (haveSameSize (bufImg1, bufImg2))
		{
			for (int x = 0; x < bufImg1.getWidth (); x++)
			{
				for (int y = 0; y < bufImg1.getHeight (); y++)
				{
					if (bufImg1.getRGB (x, y) != bufImg2.getRGB (x, y))
					{
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}
}
