
package compare;

/**
 * This Class represents a Match between two Images. So this Class holds the two
 * Images and a double representing how much thees Images match each other. A
 * matching value of 0 is absolutly possible!
 * 
 * @author Dirk Peters
 */
public class Match implements Comparable<Match>
{
	final protected ImageWithInfo imgA, imgB;
	final protected double        percentage;
	
	/**
	 * Creates new "Match"-Object with the two Images and the matching-value.
	 * 
	 * @param imgA
	 *            Image A
	 * @param imgB
	 *            Image B
	 * @param percentage
	 *            double value reaching from 0 to 1
	 */
	public Match (final ImageWithInfo imgA, final ImageWithInfo imgB, final double percentage)
	{
		this.imgA = imgA;
		this.imgB = imgB;
		this.percentage = percentage;
	}
	
	@Override
	public int compareTo (final Match o)
	{
		final double comp = percentage - o.getPercentage ();
		if (comp < 0)
		{
			return -1;
		} else if (comp > 0)
		{
			return 1;
		} else
		{
			return 0;
		}
	}
	
	/**
	 * checks if this Match contains the given "ImageWithInfo"-Object.
	 * 
	 * @param img
	 * @return true if it contains the given "ImageWithInfo"-Object.
	 */
	public boolean contains (final ImageWithInfo img)
	{
		if (imgA.equals (img) || imgB.equals (img))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the first Image from the Match
	 * 
	 * @return Image A
	 */
	public ImageWithInfo getImageA ()
	{
		return imgA;
	}
	
	/**
	 * Returns the second Image from the Match
	 * 
	 * @return Image B
	 */
	public ImageWithInfo getImageB ()
	{
		return imgB;
	}
	
	/**
	 * Returns the Match-Value from this Match. 0 = 0% 1 = 100%
	 * 
	 * @return
	 */
	public double getPercentage ()
	{
		return percentage;
	}
	
	@Override
	public String toString ()
	{
		return "<" + getImageA ().getTitle () + "> " + String.format ("%.2f%%", percentage * 100) + " <" + getImageB ()
		        .getTitle () + ">";
	}
}
