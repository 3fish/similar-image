
package compare;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * This Method holds a Image and some relevant Informations about it. The actual
 * Image data is loaded when it is demanded by the method "getBufferedImage".
 * 
 * @author Dirk Peters
 */
public class ImageWithInfo
{
	protected final File file;
	
	/**
	 * Instatiates a new Object with the Infos and Image pointet to by the given
	 * File Object.
	 * 
	 * @param file
	 *            pointer to file
	 */
	public ImageWithInfo (final File file)
	{
		this.file = file;
	}
	
	/**
	 * deletes the file from the filesystem
	 * 
	 * @return true if successful
	 */
	public boolean deleteImageFile ()
	{
		return getFile ().delete ();
	}
	
	/**
	 * returns a BufferedImage with the Image content. This method loads the
	 * Image from the Filesystem.
	 * 
	 * @return
	 * @throws IOException
	 */
	public final BufferedImage getBufferedImage () throws IOException
	{
		return ImageIO.read (file);
	}
	
	/**
	 * returns a "File"-Object pointing to the Image.
	 * 
	 * @return
	 */
	public final File getFile ()
	{
		return file;
	}
	
	/**
	 * returns a String containing a Title for this Image. Usually this is the
	 * Filename icluding its extension.
	 * 
	 * @return
	 */
	public String getTitle ()
	{
		return file.getName ();
	}
}
