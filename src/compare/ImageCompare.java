
package compare;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * An Abstract Class which needs to be extended for any class that wants to
 * implement a compare algorythm/method.
 * 
 * @author Dirk Peters
 */
public abstract class ImageCompare
{
	BufferedImage              bufImg1, bufImg2;
	public static final double MAX_DELTA_E = Math.sqrt (Math.pow (100, 2) + Math.pow (250, 2) + Math
	                                               .pow (250, 2));
	
	protected ImageCompare () throws IOException
	{}
	
	/**
	 * Creates a new "ImageCompare"-Object and loads the given Images into it.
	 * 
	 * @param img1
	 * @param img2
	 * @throws IOException
	 *             thrown if there was an error while loading the images
	 */
	public ImageCompare (final ImageWithInfo img1, final ImageWithInfo img2) throws IOException
	{
		bufImg1 = img1.getBufferedImage ();
		bufImg2 = img2.getBufferedImage ();
	}
	
	/**
	 * This Method is called by the "SearchThread" to compare two images. The
	 * expectet result should be between 0 (no Match) and 1 (100% the same
	 * image).
	 * 
	 * @return number between 0.0 and 1.0
	 */
	public abstract double compareImages ();
	
	/**
	 * This Method is called from the "SearchThread" to compare two images. This
	 * method should run a bit faster in generall than the
	 * "compareImages"-Method since it only seeks absolut matches!
	 * 
	 * @return true if the two Images are equal
	 */
	public abstract boolean isExactMatch ();
	
	/**
	 * This is a helpermethod which resizes any BufferedImage quiet fast! All
	 * child-classes should use this function for rescaling purposes.
	 * 
	 * @param src
	 *            the source Image
	 * @param width
	 *            the target width
	 * @param height
	 *            the target height
	 * @return a new BufferedImage with the resized content of the source Image
	 */
	public BufferedImage resizeImage (final BufferedImage src, final int width, final int height)
	{
		final BufferedImage img = new BufferedImage (width, height, BufferedImage.TYPE_INT_RGB);
		img.getGraphics ().drawImage (src, 0, 0, width, height, null);
		return img;
	}
}
